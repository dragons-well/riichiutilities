package com.dragonwellstudios.riichimahjongutilities.score


import android.os.Bundle
import android.support.v4.app.DialogFragment
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.CompoundButton
import android.widget.NumberPicker

import com.dragonwellstudios.riichimahjongutilities.R
import kotlinx.android.synthetic.main.fragment_manual_point_calculator_dialog.*;

/**
 * A simple [Fragment] subclass.
 */
class ManualPointCalculatorDialogFragment : DialogFragment() {
    /**
     * After 13 han a Yakuman is scored in most rule sets
     */
    private val HAN_VALUES = Array(13, { i ->
        return@Array if (i < 12) {
            (i + 1).toString()
        } else {
            return@Array "13+"
        }
    })

    /**
     * All possible fu values that are relevant
     */
    private val FU_VALUES = arrayOf("20", "25", "30", "40", "50", "60", "70", "80", "90", "100", "110", "120", "130+")
    private val FU = arrayOf(20, 25, 30, 40, 50, 60, 70, 80, 90, 100, 110, 120, 130)
    private val VALUE_CHANGED = { _: NumberPicker, _: Int, _: Int -> render(makeModel())}

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater!!.inflate(R.layout.fragment_manual_point_calculator_dialog, container, false)
    }

    private lateinit var scoreTable: ScoreTable


    private fun makeModel(): ScoreModel {
        val han = numberPicker3.value + 1
        val fu = FU[numberPicker4.value]
        val type : ScoreModel.ScoreType
        if (kirage.isChecked){

        }
        if(dealer.isChecked){
            type = if (tsumo.isChecked) ScoreModel.ScoreType.DealerTsumo else ScoreModel.ScoreType.DealerRon
        }else{
            type = if(tsumo.isChecked) ScoreModel.ScoreType.NonDealerTsumo else ScoreModel.ScoreType.NonDealerRon
        }

        return ScoreModel(scoreTable.lookupScore(han, fu), type)
    }
    override fun onStart() {
        super.onStart()
        scoreTable = ScoreTable()
        render(makeModel())
        numberPicker3.displayedValues = HAN_VALUES
        numberPicker3.maxValue = HAN_VALUES.size - 1
        numberPicker4.displayedValues = FU_VALUES
        numberPicker4.maxValue = FU_VALUES.size - 1

        numberPicker3.setOnValueChangedListener(VALUE_CHANGED)
        numberPicker4.setOnValueChangedListener(VALUE_CHANGED)

        kirage.setOnCheckedChangeListener({ _: CompoundButton, _: Boolean -> render(makeModel())})
        tsumo.setOnCheckedChangeListener({ _: CompoundButton, _: Boolean -> render(makeModel())})
        dealer.setOnCheckedChangeListener({ _: CompoundButton, _: Boolean -> render(makeModel())})
    }

    private fun render(model: ScoreModel) {
        when (model.type){
            ScoreModel.ScoreType.DealerTsumo -> {
                primaryScoreDisplay.text = getString(R.string.all_pay, model.score.tsumoDealerNonDealer)
                secondaryTextDisplay.visibility = View.GONE
            }
            ScoreModel.ScoreType.DealerRon ->{
                primaryScoreDisplay.text = getString(R.string.nondealer_pays, model.score.ronDealer)
                secondaryTextDisplay.visibility = View.GONE
            }
            ScoreModel.ScoreType.NonDealerRon -> {
                primaryScoreDisplay.text = getString(R.string.payment, model.score.ronNonDealer)
                secondaryTextDisplay.visibility = View.GONE
            }
            ScoreModel.ScoreType.NonDealerTsumo -> {
                secondaryTextDisplay.visibility = View.VISIBLE
                primaryScoreDisplay.text = getString(R.string.dealer_pays, model.score.tsumoDealerNonDealer)
                secondaryTextDisplay.text = getString(R.string.nondealer_pays, model.score.tsumoNonDealerNonDealer)
            }
        }
    }
}
