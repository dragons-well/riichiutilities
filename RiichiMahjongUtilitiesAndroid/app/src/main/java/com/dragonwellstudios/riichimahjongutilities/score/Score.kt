package com.dragonwellstudios.riichimahjongutilities.score

/**
 * @author Luke Powell
 * @param tsumoNonDealerNonDealer Tsumo payment from non-dealer to non-dealer
 * @param tsumoDealerNonDealer Tsumo payment from dealer to non-dealer, also from all when dealer tsumo
 * @param ronNonDealer Ron payment to non-dealer
 * @param ronDealer Ron payment to dealer
 */
data class Score(val tsumoNonDealerNonDealer : Int, val tsumoDealerNonDealer : Int, val ronNonDealer : Int, val ronDealer : Int)