package com.dragonwellstudios.riichimahjongutilities.score

/**
 * Created by Victus on 8/24/2017.
 */
data class ScoreModel(val score : Score, val type : ScoreType) {
    enum class ScoreType {
        DealerTsumo,
        DealerRon,
        NonDealerRon,
        NonDealerTsumo
    }
}