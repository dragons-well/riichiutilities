package com.dragonwellstudios.riichimahjongutilities.score

/**
 * Created by lukepowell on 1/1/17.
 *
 *
 * Convert a han/fu score into end score
 */
class ScoreTable {

    fun lookupScore(han: Int, fu: Int): com.dragonwellstudios.riichimahjongutilities.score.Score {
        var fu = fu

        //Round up for any fu besides chiitoitsu
        if (fu != 25) {
            fu = (Math.ceil((fu / 10.0f).toDouble()) * 10).toInt()
        }

        var basePoints: Int
        when (han) {
            1, 2, 3, 4 -> {
                //Standard equation to calculate base points
                basePoints = fu * Math.pow(2.0, (2 + han).toDouble()).toInt()
                basePoints = Math.min(basePoints, MANGAN_BASE_POINTS)
            }
            5 -> basePoints = MANGAN_BASE_POINTS
            6, 7 -> basePoints = HANEMAN_BASE_POINTS
            8, 9, 10 -> basePoints = BAIMAN_BASE_POINTS
            11, 12 -> basePoints = SANBAIMAN_BASE_POINTS
            else -> basePoints = YAKUMAN_BASE_POINTS
        }

        var ronPayoutNonDealer = 4 * basePoints
        var ronPayoutDealer = 6 * basePoints
        var tsumoDealerPayout = 2 * basePoints
        var tsumoNonDealerPayout = basePoints

        //Round to the nearest 100
        ronPayoutNonDealer = (Math.ceil((ronPayoutNonDealer / 100.0f).toDouble()) * 100.0f).toInt()
        ronPayoutDealer = (Math.ceil((ronPayoutDealer / 100.0f).toDouble()) * 100.0f).toInt()
        tsumoDealerPayout = (Math.ceil((tsumoDealerPayout / 100.0f).toDouble()) * 100.0f).toInt()
        tsumoNonDealerPayout = (Math.ceil((tsumoNonDealerPayout / 100.0f).toDouble()) * 100.0f).toInt()


        return Score(tsumoNonDealerPayout, tsumoDealerPayout, ronPayoutNonDealer, ronPayoutDealer)
    }

    companion object {
        val MANGAN_BASE_POINTS = 2000
        val HANEMAN_BASE_POINTS = 3000
        val BAIMAN_BASE_POINTS = 4000
        val SANBAIMAN_BASE_POINTS = 6000
        val YAKUMAN_BASE_POINTS = 8000
    }
}
