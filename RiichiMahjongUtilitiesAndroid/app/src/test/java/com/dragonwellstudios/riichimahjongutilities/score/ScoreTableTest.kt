package com.dragonwellstudios.riichimahjongutilities.score

import org.junit.Test

import org.junit.Assert.assertEquals

/**
 * Created by lukepowell on 1/1/17.
 */
class ScoreTableTest {
    @Test
    fun lookupScore1() {
    }

    @Test
    fun lookupScore() {
        val scoreTable = ScoreTable()

        var score = scoreTable.lookupScore(1,30)
        assertEquals(300, score.tsumoNonDealerNonDealer)
        assertEquals(500, score.tsumoDealerNonDealer)

        score = scoreTable.lookupScore(2, 30)
        assertEquals(500, score.tsumoNonDealerNonDealer)
        assertEquals(1000, score.tsumoDealerNonDealer)
        assertEquals(2000, score.ronNonDealer)
        assertEquals(2900, score.ronDealer)

    }

    companion object {

        private val han = intArrayOf(1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14)
        private val fu = intArrayOf(20, 25, 30, 40, 50, 60, 70, 80, 90, 100, 110)
    }
}